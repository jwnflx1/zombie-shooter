﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.AI;
	using UnityEngine.Events;

	public enum EnemyState
	{
		STATE_WALK,
		STATE_ATTACK
	}

	public class EnemyBase : MonoBehaviour
	{
		[SerializeField]
		protected NavMeshAgent _nav;
		[SerializeField]
		protected Animator _anim;

		protected Player _player;

		protected float _hitPoint;
		protected float _attackRange;
		protected float _moveSpeed;
		protected float _bulletDamage;

		protected EnemyState _curState;

		protected UnityAction _onKilled;

		private float _updateRate;
		private float _timer;
		
		public virtual void Init(Player player, GameConfig config, UnityAction onKilled)
		{
			_timer = 0;
			_player = player;
			_bulletDamage = config.BulletDamage;
			_onKilled = onKilled;
		}

		public void StartEnemy(Vector3 pos, float hitpoint)
		{
			_nav.Warp(pos);
			_nav.SetDestination(_player.transform.position);
			_timer = 0;
			_curState = EnemyState.STATE_WALK;

			_hitPoint = hitpoint;
		}

		// Update is called once per frame
		protected virtual void Update()
		{
			_timer += Time.deltaTime;

			if (_timer > Const.NAV_ENEMY_UPDATE_INTERVAL)
			{
				UpdatePath();
				_timer = 0;
			}

			if (_curState == EnemyState.STATE_WALK)
			{
				if (Vector3.Distance(transform.localPosition, _player.transform.position) <= _nav.stoppingDistance)
				{
					_nav.isStopped = true;
					_curState = EnemyState.STATE_ATTACK;
					_anim.SetBool(Const.ANIMATION_EATING, true);
				}
			}
		}

		public void OnFinishAttacking()
		{
			_curState = EnemyState.STATE_WALK;
			_anim.SetBool(Const.ANIMATION_EATING, false);
			_nav.isStopped = false;

			if (Vector3.Distance(transform.localPosition, _player.transform.position) <= _nav.stoppingDistance)
			{ _player.TakeDamage(); }
			
		}

		protected void UpdatePath()
		{
			_nav.SetDestination(_player.transform.position);
		}

		protected virtual void TakeDamage()
		{
			_hitPoint -= _bulletDamage;

			if(_hitPoint <= 0)
			{
				_onKilled?.Invoke();
				gameObject.SetActive(false);
			}
		}

		private void OnTriggerEnter(Collider other)
		{
			if(other.gameObject.tag == Const.TAG_BULLET)
			{
				other.gameObject.SetActive(false);
				TakeDamage();
			}
		}
	}
}