﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;

	public class NormalEnemy : EnemyBase
	{
		public override void Init(Player player, GameConfig config, UnityAction onKilled)
		{
			base.Init(player, config, onKilled);

			_attackRange = config.NormalAttackRange;
			_moveSpeed = config.NormalMoveSpeed;

			_nav.speed = _moveSpeed;
			_nav.stoppingDistance = 3f + _attackRange;
		}
	}
}