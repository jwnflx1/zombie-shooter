﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;

	public class BossEnemy : EnemyBase
	{
		[SerializeField]
		private GameObject _mesh;

		private int _formeIdx;
		private GameConfig _config;
		private UnityAction _onBossDefeated;

		public override void Init(Player player, GameConfig config, UnityAction onKilled)
		{
			base.Init(player, config, onKilled);
			_config = config;
			_formeIdx = 0;

			ChangeForme(0);
		}

		public void SetBossDefeatedAction(UnityAction onBossDefeated)
		{
			_onBossDefeated = onBossDefeated;
		}


		protected override void TakeDamage()
		{
			_hitPoint -= _bulletDamage;

			if(_hitPoint <= 0)
			{
				_formeIdx++;
				if(_formeIdx < 3)
				{ ChangeForme(_formeIdx); }
				else
				{
					_onBossDefeated?.Invoke();
					gameObject.SetActive(false);
				}
			}
		}

		private void ChangeForme(int formeIdx)
		{
			_hitPoint = _config.BossHitpoints[formeIdx];
			_nav.stoppingDistance = 3f + _config.BossAttackRange[_formeIdx];

			_moveSpeed = _config.BossMoveSpeed[_formeIdx];
			_nav.speed = _moveSpeed;

			_mesh.transform.localScale = Vector3.one * _config.SizeScaling[_formeIdx];
		}
	}
}