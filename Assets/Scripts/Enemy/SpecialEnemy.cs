﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;

	public class SpecialEnemy : EnemyBase
	{
		private float _nearExplodingSpeed;
		private float _explodeTimer;
		private float _updatePathCounter;
		private float _secToExplode;
		private bool _isExplodingState;

		public override void Init(Player player, GameConfig config, UnityAction onKilled)
		{
			base.Init(player, config, onKilled);

			_attackRange = config.SpecialAttackRange;
			_moveSpeed = config.SpecialMoveSpeed;
			_nearExplodingSpeed = _moveSpeed * 2f;
			_secToExplode = config.SecondsToExplode;

			_nav.speed = _moveSpeed;
			_nav.stoppingDistance = 3f + _attackRange;
			_isExplodingState = false;
		}

		protected override void Update()
		{
			if (_isExplodingState)
			{
				_explodeTimer += Time.deltaTime;

				if(_explodeTimer > _updatePathCounter)
				{
					UpdatePath();
					_updatePathCounter += Const.NAV_ENEMY_UPDATE_INTERVAL;
				}

				if(_explodeTimer > _secToExplode)
				{
					gameObject.SetActive(false);
					if (Vector3.Distance(transform.localPosition, _player.transform.position) <= _nav.stoppingDistance)
					{ _player.TakeDamage(); }
					_isExplodingState = false;
					_moveSpeed /= 2;
					_nav.speed = _moveSpeed;
				}
			}
			else
			{ base.Update(); }
		}

		protected override void TakeDamage()
		{
			_hitPoint -= _bulletDamage;

			if(_hitPoint <= 0 && _isExplodingState == false)
			{
				TriggerExploding();
			}
		}

		private void TriggerExploding()
		{
			_isExplodingState = true;
			_nav.speed = _nearExplodingSpeed;
			_explodeTimer = 0;
			_updatePathCounter = Const.NAV_ENEMY_UPDATE_INTERVAL;
			UpdatePath();

			_onKilled?.Invoke();
		}
	}
}