﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class EnemyAnimationEvents : MonoBehaviour
	{
		public void OnFinishAttacking()
		{
			EnemyBase enemy = transform.parent.GetComponent<EnemyBase>();
			if (enemy)
			{ enemy.OnFinishAttacking(); }
		}
	}
}