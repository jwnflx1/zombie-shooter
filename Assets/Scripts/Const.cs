﻿namespace zombieshooter
{

	public static class Const
	{
		public const float NAV_ENEMY_UPDATE_INTERVAL = 1f;

		public const string TAG_BOUNDARIES = "Boundaries";
		public const string TAG_ENEMY = "Enemy";
		public const string TAG_BULLET = "Bullet";

		public const string ANIMATION_EATING = "IsEating";
		public const string ANIMATION_MOVE = "Speed_f";
		public const string ANIMATION_DEATH = "Death_b";
	}
}