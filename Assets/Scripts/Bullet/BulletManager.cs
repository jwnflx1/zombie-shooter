﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;

	public enum GunState
	{
		STATE_CANSHOOT,
		STATE_RELOADING
	}
	public class BulletManager: MonoBehaviour
	{
		[SerializeField]
		private Bullet _bulletPrefab;
		[SerializeField]
		private Transform _bulletParent;

		private float _timer;
		private float _reloadTimer;
		private float _bulletSpeed;
		private float _interval;
		private float _reloadTime;
		private GameObject _playerMesh;
		private List<Bullet> _bulletList;

		private GunState _curState;

		private int _curBullet;
		private int _maxBulletInClip;

		private UnityAction _onReload;
		private UnityAction _onFinishReloading;
		private UnityAction<int> _bulletUpdate;

		public bool IsShooting { get; private set; }
		public void Awake()
		{
			_bulletList = new List<Bullet>();
		}

		public void Start()
		{
			IsShooting = false;
			_timer = 0;
			_reloadTimer = 0;
			_curState = GunState.STATE_CANSHOOT;
		}

		public void Init(GameObject player, GameConfig config, UnityAction<int> bulletUpdate, UnityAction onReload, UnityAction onFinishReloading)
		{
			_playerMesh = player;
			_bulletSpeed = config.BulletSpeed;
			_interval = config.BulletInterval;
			_maxBulletInClip = config.BulletPerClip;
			_reloadTime = config.ReloadSpeed;
			_onReload = onReload;
			_bulletUpdate = bulletUpdate;
			_onFinishReloading = onFinishReloading;

			_curBullet = _maxBulletInClip;
			_timer = _interval;
		}

		public void Update()
		{
			if (_curState == GunState.STATE_RELOADING)
			{
				_reloadTimer += Time.deltaTime;

				if (_reloadTimer >= _reloadTime)
				{
					_curBullet = _maxBulletInClip;
					_onFinishReloading?.Invoke();
					_reloadTimer = 0;
					_curState = GunState.STATE_CANSHOOT;
				}
			}

			if (IsShooting)
			{
				if (_curBullet > 0)
				{
					if (_curState == GunState.STATE_CANSHOOT)
					{
						_timer += Time.deltaTime;

						if (_timer > _interval)
						{
							Shoot();
							_timer = 0;
						}
					}
				}
				else
				{
					if (_curState == GunState.STATE_CANSHOOT)
					{
						_curState = GunState.STATE_RELOADING;
						_onReload?.Invoke();
					}
				}
			}
		}

		public void StartShooting()
		{
			IsShooting = true;
		}

		public void StopShooting()
		{
			IsShooting = false;
			_timer = 0;
		}

		private void Shoot()
		{
			Bullet bullet = GetOrCreateBullet();
			bullet.Init(_playerMesh, _bulletSpeed);
			_curBullet--;
			_bulletUpdate?.Invoke(_curBullet);
		}

		private Bullet GetOrCreateBullet()
		{
			if (_bulletList.Count > 0)
			{
				for (int i = 0; i < _bulletList.Count; i++)
				{
					if(_bulletList[i].gameObject.activeSelf == false)
					{
						_bulletList[i].gameObject.SetActive(true);
						return _bulletList[i];
					}
				}
			}

			Bullet bullet = Instantiate(_bulletPrefab, _bulletParent);
			_bulletList.Add(bullet);
			return bullet;
		}
	}
}