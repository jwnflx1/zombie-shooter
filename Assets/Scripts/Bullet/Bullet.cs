﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Bullet : MonoBehaviour
	{
		private Vector3 _forward;
		private float _speed;
		public void Init(GameObject playerMesh, float speed)
		{
			_speed = speed;
			_forward = playerMesh.transform.forward;
			transform.localPosition = playerMesh.transform.parent.position;
		}

		// Update is called once per frame
		void FixedUpdate()
		{
			transform.Translate(_forward * _speed * Time.deltaTime);
		}

		private void OnTriggerEnter(Collider other)
		{
			if (other.gameObject.tag == Const.TAG_BOUNDARIES)
			{
				gameObject.SetActive(false);
			}
		}
	}
}