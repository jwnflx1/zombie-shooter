﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.SceneManagement;
	using UnityEngine.UI;

	public enum CameraMode
	{
		MODE_FIRSTPERSON = 0,
		MODE_THIRDPERSON = 1
	}
	public class GameManager : MonoBehaviour
	{
		[SerializeField]
		private References _references;

		private bool _hasMouseDown;
		private CameraMode _cameraMode;
		private Control _control;

		private Player _player;

		private void Awake()
		{
			_references.cameraSwitchButton.onClick.AddListener(OnSwitchCamera);
			_references.restartButton.onClick.AddListener(OnRestart);
		}

		// Start is called before the first frame update
		void Start()
		{
			_cameraMode = CameraMode.MODE_THIRDPERSON;
			_player = _references.player;
			_references.spawner.Init(_references.gameConfig, _player, OnBossDefeated);

			InitUI();
			_player.Init(_references.gameConfig, UpdateHealth, UpdateBullet, OnReloading, OnFinishReloading, OnPlayerDeath);
			_control = new Control(_player, _references.movementJoystick, _references.shootJoystick);

			_references.finishLabel.gameObject.SetActive(false);
		}

		// Update is called once per frame
		void Update()
		{
			_control.UpdateControl();
		}

		private void InitUI()
		{
			_references.healthLabel.text = _references.gameConfig.PlayerLives.ToString();
			_references.bulletLabel.text = _references.gameConfig.BulletPerClip.ToString();
		}

		private void UpdateHealth(int curHealth)
		{
			_references.healthLabel.text = curHealth.ToString();
		}

		private void UpdateBullet(int curBullet)
		{
			_references.bulletLabel.text = curBullet.ToString();
		}

		private void OnReloading()
		{
			_references.bulletLabel.text = "Reloading..";
		}

		private void OnFinishReloading()
		{
			_references.bulletLabel.text = _references.gameConfig.BulletPerClip.ToString();
		}

		private void OnBossDefeated()
		{
			_references.finishLabel.gameObject.SetActive(true);
			_control.DisableControl();
		}

		private void OnPlayerDeath()
		{
			_references.finishLabel.gameObject.SetActive(true);
			_references.movementJoystick.HideJoystick();
			_references.shootJoystick.HideJoystick();
			_control.DisableControl();
		}

		private void OnRestart()
		{
			SceneManager.LoadScene(0);
		}

		private void OnSwitchCamera()
		{
			if(_cameraMode == CameraMode.MODE_FIRSTPERSON)
			{
				_player.SwitchToThirdPersonCam();

				_cameraMode = CameraMode.MODE_THIRDPERSON;
			}
			else
			{
				_player.SwitchToFirstPersonCam();

				_cameraMode = CameraMode.MODE_FIRSTPERSON;
			}
		}
	}
}