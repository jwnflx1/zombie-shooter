﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

	public class FpsCounter : MonoBehaviour
	{
		[SerializeField]
		private Text _display;

		private int _frameRate;
		
		// Update is called once per frame
		void Update()
		{
			_frameRate = (int)(1f / Time.unscaledDeltaTime);

			_display.text = string.Format("fps: {0}",_frameRate.ToString());
		}
	}
}