﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;

	public class Player : MonoBehaviour
	{
		[SerializeField]
		private GameObject _playerMesh;
		[SerializeField]
		private BulletManager _bulletManager;
		[SerializeField]
		private Transform _camera;
		[SerializeField]
		private Transform _firstPersonCamPos;
		[SerializeField]
		private Transform _thirdPersonCamPos;
		[SerializeField]
		private Animator _anim;
		[SerializeField]
		private Rigidbody _rigidbody;

		private int _lives;
		private bool _isFirstPersonCamera;
		private float _speed;

		private UnityAction<int> _healthUpdate;
		private UnityAction _onPlayerDeath;

		public bool IsShooting
		{
			get
			{
				return _bulletManager.IsShooting;
			}
			private set { }
		}

		// Start is called before the first frame update
		void Start()
		{
			_isFirstPersonCamera = false;
		}

		public void Init(GameConfig config, UnityAction<int> HealthUpdate, UnityAction<int> BulletUpdate, UnityAction onReloading, UnityAction onFinishReloading, UnityAction onPlayerDeath)
		{
			_speed = config.MoveSpeed;
			_lives = config.PlayerLives;
			_healthUpdate = HealthUpdate;
			_onPlayerDeath = onPlayerDeath;
			_bulletManager.Init(_playerMesh, config, BulletUpdate, onReloading, onFinishReloading);
		}

		// Update is called once per frame
		void FixedUpdate()
		{
#if UNITY_EDITOR
			Move(new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));
#endif
		}

		public void Move(Vector3 dir)
		{
			//transform.Translate(dir * _speed * Time.deltaTime);
			_rigidbody.velocity = dir * _speed * 100 * Time.deltaTime;
			_anim.SetFloat(Const.ANIMATION_MOVE, _rigidbody.velocity.magnitude);
		}

		public void Stop()
		{
			Move(Vector3.zero);
		}

		public void Rotate(Vector3 dir)
		{
			Quaternion target;
			if (_isFirstPersonCamera)
			{
				Vector3 angles = transform.rotation.eulerAngles + dir;
				target = Quaternion.LookRotation(-dir, Vector3.up);

				transform.rotation = target;
			}
			else
			{
				target = Quaternion.LookRotation(dir, Vector3.up);
				_playerMesh.transform.rotation = target;
			}
		}

		public void StartShooting()
		{
			_bulletManager.StartShooting();
		}

		public void StopShooting()
		{
			_bulletManager.StopShooting();
		}

		public void TakeDamage()
		{
			_lives--;
			_healthUpdate?.Invoke(_lives);
			if (_lives <= 0)
			{
				//GAMEOVER
				Debug.Log("GAMEOVER");
				_anim.SetBool(Const.ANIMATION_DEATH, true);
				_onPlayerDeath?.Invoke();
			}
		}

		public void SwitchToFirstPersonCam()
		{
			_camera.localPosition = _firstPersonCamPos.localPosition;
			_camera.localRotation = _firstPersonCamPos.localRotation;
			_isFirstPersonCamera = true;
		}

		public void SwitchToThirdPersonCam()
		{
			_camera.localPosition = _thirdPersonCamPos.localPosition;
			_camera.localRotation = _thirdPersonCamPos.localRotation;
			_isFirstPersonCamera = false;
		}
	}
}