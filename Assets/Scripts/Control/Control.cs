﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	public class Control
	{
		private Joystick _movementJoystick;
		private Joystick _shootingJoystick;

		private bool _lockControl;

		public Control(Player player, Joystick movementJoystick, Joystick shootingJoystick)
		{
			_movementJoystick = movementJoystick;
			_shootingJoystick = shootingJoystick;

			_movementJoystick.InitMovement(player);
			_shootingJoystick.InitShooting(player);

			_lockControl = false;

			EnableControl();
		}

		public void DisableControl()
		{
			_lockControl = true;
		}

		public void EnableControl()
		{
			_lockControl = false;
		}

		public void UpdateControl()
		{
			if(_lockControl)
			{ return; }

#if UNITY_EDITOR
			if (Input.GetMouseButtonDown(0))
			{
				if (Input.mousePosition.y < (Screen.height * 0.8))
				{
					if (_movementJoystick.IsTouching == false)
					{
						if (Input.mousePosition.x < (Screen.width * 0.5))
						{ _movementJoystick.ShowJoystick(Input.mousePosition); }
					}

					if (_shootingJoystick.IsTouching == false)
					{
						if (Input.mousePosition.x > (Screen.width * 0.5))
						{ _shootingJoystick.ShowJoystick(Input.mousePosition); }
					}
				}
			}

			if (Input.GetMouseButton(0))
			{
				if (_movementJoystick.IsTouching == true)
				{
					_movementJoystick.UpdateJoystickPos(Input.mousePosition);
				}

				if (_shootingJoystick.IsTouching == true)
				{
					_shootingJoystick.UpdateJoystickPos(Input.mousePosition);
				}
			}

			if (Input.GetMouseButtonUp(0))
			{
				if (_movementJoystick.IsTouching == true)
				{
					_movementJoystick.HideJoystick();
				}

				if (_shootingJoystick.IsTouching == true)
				{
					_shootingJoystick.HideJoystick();
				}
			}
#else

			if (Input.touchCount > 0)
			{
				Touch[] touches = Input.touches;

				for (int i = 0; i < Input.touchCount; i++)
				{
					if (i > 1)
					{ break;}

					Vector2 pos = touches[i].position;
					if(pos.y > (Screen.height * 0.8))
					{ continue; }

					if (touches[i].phase == TouchPhase.Began)
					{
						if (_movementJoystick.IsTouching == false)
						{
							if (pos.x < (Screen.width * 0.5))
							{
								_movementJoystick.ShowJoystick(pos);
								_movementJoystick.TouchId = touches[i].fingerId;
							}
						}

						if (_shootingJoystick.IsTouching == false)
						{
							if (pos.x > (Screen.width * 0.5))
							{
								_shootingJoystick.ShowJoystick(pos);
								_shootingJoystick.TouchId = touches[i].fingerId;
							}
						}
					}
					else if (touches[i].phase == TouchPhase.Moved)
					{
						if (_movementJoystick.IsTouching == true)
						{
							if (_movementJoystick.TouchId == touches[i].fingerId)
							{ _movementJoystick.UpdateJoystickPos(touches[i].position); }
						}

						if (_shootingJoystick.IsTouching == true)
						{
							if (_shootingJoystick.TouchId == touches[i].fingerId)
							{ _shootingJoystick.UpdateJoystickPos(touches[i].position); }
						}
					}
					else if (touches[i].phase == TouchPhase.Ended)
					{
						if (_movementJoystick.IsTouching == true)
						{
							if (_movementJoystick.TouchId == touches[i].fingerId)
							{ _movementJoystick.HideJoystick(); }
						}

						if (_shootingJoystick.IsTouching == true)
						{
							if (_shootingJoystick.TouchId == touches[i].fingerId)
							{ _shootingJoystick.HideJoystick(); }
						}
					}
				}
			}
#endif
		}
	}
}