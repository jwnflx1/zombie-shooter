﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.EventSystems;

	public class Joystick : MonoBehaviour
	{
		[SerializeField]
		private Image _bgImg;
		[SerializeField]
		private Image _controlImg;

		private Player _controlledChar;
		private Vector3 _input;
		private bool _isMovementJoystick;
		public bool IsTouching { get; private set; }

		public int TouchId;

		private Vector2 _touchPos;

		public void InitMovement(Player player)
		{
			_isMovementJoystick = true;
			_controlledChar = player;
		}

		public void InitShooting(Player player)
		{
			_isMovementJoystick = false;
			_controlledChar = player;
		}

		public void ShowJoystick(Vector3 pos)
		{
			gameObject.SetActive(true);
			gameObject.transform.position = pos;

			IsTouching = true;
		}

		public void HideJoystick()
		{
			IsTouching = false;
			_input = Vector3.zero;
			_controlImg.rectTransform.anchoredPosition = Vector3.zero;

			if(_isMovementJoystick)
			{ _controlledChar.Stop(); }
			else
			{ _controlledChar.StopShooting(); }

			gameObject.SetActive(false);
		}

		public void UpdateJoystickPos(Vector2 pos)
		{
			_touchPos = pos;
		}

		private void Update()
		{
			if(IsTouching)
			{
				if (RectTransformUtility.ScreenPointToLocalPointInRectangle(_bgImg.rectTransform, _touchPos, null, out Vector2 pos))
				{
					pos.x = pos.x / _bgImg.rectTransform.sizeDelta.x;
					pos.y = pos.y / _bgImg.rectTransform.sizeDelta.y;

					_input = new Vector3(pos.x, 0, pos.y);
					_input = (_input.magnitude > 1.0f) ? _input.normalized : _input;

					_controlImg.rectTransform.anchoredPosition = new Vector2(_input.x * (_bgImg.rectTransform.sizeDelta.x / 3), _input.z * (_bgImg.rectTransform.sizeDelta.y / 3));
				}
			}
		}

		private void FixedUpdate()
		{
			if(IsTouching)
			{
				Vector3 dir = new Vector3(_input.x, 0, _input.z);
				if (_isMovementJoystick)
				{
					_controlledChar.Move(dir);
				}
				else
				{
					if(!_controlledChar.IsShooting)
					{ _controlledChar.StartShooting();}

					_controlledChar.Rotate(dir);
				}
			}
		}
	}
}