﻿
namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;

	[CreateAssetMenu(fileName = "GameConfig", menuName = "Data/CreateConfig")]
	public class GameConfig : ScriptableObject
	{
		[Header("Player Config")]
		public int PlayerLives;
		public float ReloadSpeed;
		public float MoveSpeed;

		[Header("Normal Enemy Config")]
		public float[] NormalHitpoints;
		public float NormalAttackRange;
		public float NormalMoveSpeed;

		[Header("Special Enemy Config")]
		public float SpecialHitpoints;
		public float SpecialAttackRange;
		public float SpecialMoveSpeed;
		public float SecondsToExplode;

		[Header("Boss Enemy Config")]
		public float[] BossHitpoints;
		public float[] BossAttackRange;
		public float[] BossMoveSpeed;
		public float[] SizeScaling;

		[Header("Spawn Config")]
		public float FirstSpawnInterval = 5f;
		public float SpawnInterval = 3f;

		[Header("Bullet Config")]
		public float BulletSpeed = 10f;
		public float BulletInterval = 1f;
		public float BulletDamage = 40f;
		public int BulletPerClip = 24;

		[Header("Wave Config")]
		public int[] MaxEnemyOnMapBetweenWave;
		public int[] EnemyKillCountToProceed;
		public WaveData[] Waves;
	}

	[System.Serializable]
	public class WaveData
	{
		public LocationData[] Locations;
	}

	[System.Serializable]
	public class LocationData
	{
		public int NormalZombieCount;
		public int SpecialZombieCount;
	}
}