﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.Events;

	public class Spawner : MonoBehaviour
	{
		public Transform Parent;
		[Header("Spawn Points")]
		public Transform[] SpawnPoints;
		public Transform BossSpawnPoint;
		[Header("Enemy Prefabs")]
		public NormalEnemy[] NormalEnemyPrefabs;
		public SpecialEnemy[] SpecialEnemyPrefabs;
		public BossEnemy[] BossEnemyPrefabs;

		private List<NormalEnemy> _normalEnemyList;
		private List<SpecialEnemy> _specialEnemyList;
		private List<BossEnemy> _bossEnemyList;

		private Player _player;
		private GameConfig _config;

		[SerializeField]
		private int _curWaveIdx;
		[SerializeField]
		private int _totalEnemyDefeated;
		[SerializeField]
		private int _totalEnemyToFinishWave;
		[SerializeField]
		private int _enemyOnMap;
		[SerializeField]
		private bool _isWavePhase; 

		private float _spawnTimer;
		private float _spawnRate;
		private float _spawnCount;

		private UnityAction _onBossDefeated;

		private void Awake()
		{
			_normalEnemyList = new List<NormalEnemy>();
			_specialEnemyList = new List<SpecialEnemy>();
			_bossEnemyList = new List<BossEnemy>();
		}

		public void Init(GameConfig config, Player player, UnityAction onBossDefeated)
		{
			_config = config;
			_player = player;
			_spawnRate = _config.FirstSpawnInterval;
			_spawnCount = 0;
			_curWaveIdx = 0;
			_isWavePhase = false;
			_onBossDefeated = onBossDefeated;


			CreateEnemyPool();
		}

		private void CheckForEnemyKill()
		{
			_totalEnemyDefeated++;

			if (_isWavePhase)
			{
				Debug.Log(_totalEnemyDefeated);
				if(_totalEnemyDefeated >= _totalEnemyToFinishWave)
				{
					_curWaveIdx++;
					_isWavePhase = false;
					_totalEnemyDefeated = 0;
					_spawnCount = 0;
					if(_curWaveIdx > 2)
					{
						//bosstime
						Spawn(BossSpawnPoint.localPosition, 2);
					}
				}
			}
			else
			{
				_enemyOnMap--;
				_totalEnemyToFinishWave = 0;

				if (_curWaveIdx >= 3)
				{ return; }

				if (_totalEnemyDefeated >= _config.EnemyKillCountToProceed[_curWaveIdx])
				{
					_isWavePhase = true;
					_totalEnemyDefeated = 0;

					WaveData waveData = _config.Waves[_curWaveIdx];
					for (int i = 0; i < waveData.Locations.Length; i++)
					{
						_totalEnemyToFinishWave += waveData.Locations[i].NormalZombieCount;
						_totalEnemyToFinishWave += waveData.Locations[i].SpecialZombieCount;
					}

					Debug.Log("WAVE " + _curWaveIdx + " TOTAL KILL NEEDED: " + _totalEnemyToFinishWave);

					_enemyOnMap = 0;
					SpawnWave();
				}
			}
		}

		// Update is called once per frame
		void Update()
		{
			_spawnTimer += Time.deltaTime;

			if (_isWavePhase)
			{

			}
			else
			{
				if (_spawnTimer > _spawnRate)
				{
					if (_curWaveIdx < 3)
					{
						if (_spawnCount < _config.EnemyKillCountToProceed[_curWaveIdx])
						{
							if (_enemyOnMap < _config.MaxEnemyOnMapBetweenWave[_curWaveIdx])
							{
								if (_spawnRate == _config.FirstSpawnInterval)
								{ _spawnRate = _config.SpawnInterval; }

								Spawn();
								_spawnTimer = 0;
							}
						}
					}
				}
			}
		}

		private Transform GetRandomSpawnPoint()
		{
			List<Transform> points = new List<Transform>();
			for(int i = 0; i < SpawnPoints.Length; i++)
			{
				if(Vector3.Distance(_player.transform.position, SpawnPoints[i].position) > 20)
				{
					points.Add(SpawnPoints[i]);
				}
			}

			return points[Random.Range(0, points.Count)];
		}

		private void Spawn()
		{
			Transform points = GetRandomSpawnPoint();
			Spawn(points.localPosition);
			_enemyOnMap++;
			_spawnCount++;
		}

		private void Spawn(Vector3 loc, int enemyType = 0)
		{
			NormalEnemy enemy;
			SpecialEnemy special;
			BossEnemy boss;

			switch (enemyType)
			{
				case 0:
					enemy = CreateOrGetNormalEnemy();
					enemy.StartEnemy(loc, (_config.NormalHitpoints[Random.Range(0, _config.NormalHitpoints.Length)] + (_curWaveIdx * 50f)));
					break;
				case 1:
					special = CreateOrGetSpecialEnemy();
					special.StartEnemy(loc, _config.SpecialHitpoints + (_curWaveIdx * 50f));
					break;
				case 2:
					boss = GetBossEnemy();
					boss.StartEnemy(loc, _config.BossHitpoints[0]);
					break;
				default:
					enemy = CreateOrGetNormalEnemy();
					enemy.StartEnemy(loc, _config.NormalHitpoints[Random.Range(0, _config.NormalHitpoints.Length)] + (_curWaveIdx * 50f));
					break;
			}
		}

		private void SpawnWave()
		{
			ShuffleSpawnPoints();
			WaveData waveData = _config.Waves[_curWaveIdx];

			for(int i = 0; i < waveData.Locations.Length; i++)
			{
				for(int j = 0; j< waveData.Locations[i].NormalZombieCount; j++)
				{
					Spawn(SpawnPoints[i].localPosition, 0);
				}

				for(int k = 0; k < waveData.Locations[i].SpecialZombieCount; k++)
				{
					Spawn(SpawnPoints[i].localPosition, 1);
				}
			}
		}

		private void CreateEnemyPool()
		{
			NormalEnemy enemy;
			SpecialEnemy special;
			BossEnemy boss;
			for(int i =0; i < 5; i++)
			{
				enemy = Instantiate(NormalEnemyPrefabs[Random.Range(0, NormalEnemyPrefabs.Length)], Parent);
				enemy.gameObject.SetActive(false);
				enemy.Init(_player, _config, CheckForEnemyKill);
				_normalEnemyList.Add(enemy);
			}

			special = Instantiate(SpecialEnemyPrefabs[Random.Range(0, SpecialEnemyPrefabs.Length)], Parent);
			special.gameObject.SetActive(false);
			special.Init(_player, _config, CheckForEnemyKill);
			_specialEnemyList.Add(special);

			boss = Instantiate(BossEnemyPrefabs[Random.Range(0, BossEnemyPrefabs.Length)], Parent);
			boss.gameObject.SetActive(false);
			boss.Init(_player, _config, CheckForEnemyKill);
			boss.SetBossDefeatedAction(_onBossDefeated);
			_bossEnemyList.Add(boss);
		}

		private NormalEnemy CreateOrGetNormalEnemy()
		{
			for(int i = 0; i < _normalEnemyList.Count; i++)
			{
				if(_normalEnemyList[i].gameObject.activeSelf == false)
				{
					_normalEnemyList[i].gameObject.SetActive(true);
					return _normalEnemyList[i];
				}
			}

			NormalEnemy enemy = Instantiate(NormalEnemyPrefabs[Random.Range(0, NormalEnemyPrefabs.Length - 1)], Parent);
			enemy.Init(_player, _config, CheckForEnemyKill);
			_normalEnemyList.Add(enemy);

			return enemy;
		}

		private SpecialEnemy CreateOrGetSpecialEnemy()
		{
			for (int i = 0; i < _specialEnemyList.Count; i++)
			{
				if(_specialEnemyList[i].gameObject.activeSelf == false)
				{
					_specialEnemyList[i].gameObject.SetActive(true);
					return _specialEnemyList[i];
				}
			}

			SpecialEnemy enemy = Instantiate(SpecialEnemyPrefabs[Random.Range(0, SpecialEnemyPrefabs.Length)], Parent);
			enemy.Init(_player, _config, CheckForEnemyKill);
			_specialEnemyList.Add(enemy);

			return enemy;
		}

		private BossEnemy GetBossEnemy()
		{
			_bossEnemyList[0].gameObject.SetActive(true);
			_bossEnemyList[0].Init(_player, _config, CheckForEnemyKill);
			return _bossEnemyList[0];
		}

		private void ShuffleSpawnPoints()
		{
			for(int i = 0; i < SpawnPoints.Length; i++)
			{
				int idx = Random.Range(i, SpawnPoints.Length);
				Transform temp = SpawnPoints[i];
				SpawnPoints[i] = SpawnPoints[idx];
				SpawnPoints[idx] = temp;
			}
		}
	}
}