﻿namespace zombieshooter
{
	using System.Collections;
	using System.Collections.Generic;
	using UnityEngine;
	using UnityEngine.UI;

	public class References : MonoBehaviour
	{
		public GameConfig gameConfig;
		public Player player;
		public Spawner spawner;

		public Joystick movementJoystick;
		public Joystick shootJoystick;

		[Header("UI")]
		public Button cameraSwitchButton;
		public Text bulletLabel;
		public Text healthLabel;

		public Text finishLabel;
		public Button restartButton;
	}
}